from __future__ import print_function, division, absolute_import


from toolz import map as m, curry
m = curry(m)

def foreach(f, seq):
    for p in seq:
        f(p)

def generate_1():
    props = ['message', 'title', 'cell_cards', 'surface_cards', 'control_cards', 'remainder']
    formats = ["""
    @property
    def {subst}(self):
        return self.__{subst}
    """,
    """
    @{subst}.setter
    def {subst}(self, {subst}):
        self.__{subst} = {subst}
    """]
    for fmt in formats:
        foreach(print, m(lambda p: fmt.format(subst=p), props))

if __name__ == '__main__':
    generate_1()
