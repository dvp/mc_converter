#!/usr/bin/env python
"""Setup script for the mc_converter module distribution."""
from distutils.core import setup
import os.path
import  mc_converter.release as release
LONG_DESCRIPTION = open('README.rst').read()

modules = ["mc_converter", ]
scripts = [os.path.join('scripts', f) for f in os.listdir('scripts')]
scripts = [s for s in scripts if (os.name == 'nt' and s.endswith('.bat'))
                                 or (os.name != 'nt' and
                                     not s.endswith('.bat'))] 
setup(# Distribution meta-data
    name=release.DISTNAME,
    version=release.MC_CONVERTER_VERSION,
    maintainer=release.MAINTAINER,
    maintainer_email=release.MAINTAINER_EMAIL,
    description=release.DESCRIPTION,
    long_description=LONG_DESCRIPTION,
    author=release.MAINTAINER,
    author_email=release.MAINTAINER_EMAIL,
    url=release.URL,
    download_url=release.URL,
    license="MIT License",
    py_modules=modules,
    scripts=scripts,
    classifiers=[
        'Development Status :: 0 - Development/Started',
        'Intended Audience :: Developers',
        'Intended Audience :: Neutronics modelling',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        # 'Programming Language :: Python :: 2.6',
        'Programming Language :: Python :: 2.7',
        # 'Programming Language :: Python :: 3',
        # 'Programming Language :: Python :: 3.0',
        # 'Programming Language :: Python :: 3.1',
        # 'Programming Language :: Python :: 3.2',
        'Programming Language :: Python :: 3.3',
        ]
    )

