# -*- coding: utf-8 -*-
#!/usr/bin/env python
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
# Dmitri Portnov: dmitri_portnov@yahoo.com
# History:
#    2015/01/25 DVP created
#    2015/01/31 DVP Added creation of output in arbitrary directory.
# TODO:
#    Add recursive inclusion and pragmas with additional code to execute  in input files
"""
"""
from __future__ import print_function, division, absolute_import

__author__ = 'Dmitri Portnov'
__updated__ = '2015-01-29'
__version__ = '0.0.3'
__rights__ = 'Copyright (c) 2015 Dmitri Portnov'

import logging
import os

import arrow as a



class InputManager:
    logger = logging.getLogger(__name__)

    def __init__(self, out_file_name):
        self._out_file_name = out_file_name  # if isinstance(_out_file_name, Path) else Path(_out_file_name)
        t = a.now()
        self._props = {'__date__' : t.format('YYYY-MM-DD'), '__time__' : t.format('HH-mm-ss')}

    @property
    def props(self):
        return self._props;

    def __enter__(self):
        InputManager.logger.debug("Opening output file '%s'" % self._out_file_name)
        dir_ = os.path.dirname(self._out_file_name)
        if dir_ and not os.path.exists(dir_):
            os.makedirs(dir_)
        self._out_file = open(self._out_file_name, 'w')
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        InputManager.logger.debug("Closing output file '%s'" % self._out_file_name)
        self._out_file.close()

    def include(self, file_):
        InputManager.logger.debug("Processing input file %s" % file_)
        with open(file_) as f:
            text = f.read()
            self._props['__file__'] = file_
            self.transform(text)

    def transform(self, text):
        transformed = text.format(**self._props)
        self.write(transformed)

    def write(self, text):
        print(text, sep='', end='', file=self._out_file)

def run_scripts(out_file_name, *ctl_files):
    logger = logging.getLogger(__name__)
    with InputManager(out_file_name) as imgr:
        context = {}
        context['imgr'] = imgr
        for filename in ctl_files:
            with open(filename) as t:
                logger.info("Executing script '%s'" % (filename))
                src = t.read()
                exec(compile(src, filename, "exec"), context)


__all__ = [InputManager, run_scripts]
