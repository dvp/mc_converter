"""
    Default logging configuration.

    Configures standard logging to console and file with INFO log level.
"""
# import logging
import logging.handlers

file_log_level = logging.INFO
console_log_level = logging.INFO
logger = logging.getLogger()
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
logger.setLevel(file_log_level)
fh = logging.handlers.RotatingFileHandler('mc_converter.log', mode='w', maxBytes=1000000, backupCount=5, encoding='utf8', delay=1000)
fh.setLevel(file_log_level)
fh.setFormatter(formatter)
logger.addHandler(fh)
ch = logging.StreamHandler()
ch.setLevel(int(console_log_level))
ch.setFormatter(formatter)
logger.addHandler(ch)
