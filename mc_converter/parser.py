from __future__ import print_function, division, absolute_import

class MCNPParseError(Exception):
    pass

class SectionParseError(MCNPParseError):
    pass


class mcnp_input_sections(object):
    def __init__(self, title, cell_cards, surface_cards, control_cards, message=None, remainder=None):
        object.__init__(self)
        self._title = title
        self._cell_cards = cell_cards
        self._surface_cards = surface_cards
        self._control_cards = control_cards
        self._message = message
        self._remainder = remainder

    @property
    def message(self):
        return self._message


    @property
    def title(self):
        return self._title


    @property
    def cell_cards(self):
        return self._cell_cards


    @property
    def surface_cards(self):
        return self._surface_cards


    @property
    def control_cards(self):
        return self._control_cards


    @property
    def remainder(self):
        return self._remainder



class mcnp_input_sections_builder(mcnp_input_sections):

    def __init__(self):
        mcnp_input_sections.__init__(self, None, None, None, None, None, None)

    @mcnp_input_sections.message.setter
    def message(self, message):
        self._message = message


    @mcnp_input_sections.title.setter
    def title(self, title):
        self._title = title


    @mcnp_input_sections.cell_cards.setter
    def cell_cards(self, cell_cards):
        self._cell_cards = cell_cards


    @mcnp_input_sections.surface_cards.setter
    def surface_cards(self, surface_cards):
        self._surface_cards = surface_cards


    @mcnp_input_sections.control_cards.setter
    def control_cards(self, control_cards):
        self._control_cards = control_cards


    @mcnp_input_sections.remainder.setter
    def remainder(self, remainder):
        self._remainder = remainder

    def build(self):
        assert not self.title is None
        if self.message is None:
            initial_run = True
        elif self.message.lower().startswith('message:'):
            initial_run = True
        elif self.message.lower().startswith('continue'):
            initial_run = False
        else:
            raise SectionParseError('Invalid content of message block: %s' % (self.message))

        if initial_run:
            assert not self.surface_cards is None
            assert not self.cell_cards is None
            assert not self.control_cards is None
        else:
            assert self.surface_cards is None
            assert self.cell_cards is None

        return mcnp_input_sections(self.title, self.cell_cards, self.surface_cards, self.control_cards, self.message, self.remainder)


def parse_sections(f):
    raise NotImplemented('parse_sections')
