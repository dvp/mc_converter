from __future__ import print_function, division, absolute_import
import os.path

if os.path.exists('logging.cfg'):
    import logging.config
    logging.config.fileConfig('logging.cfg')
else:
    import logging
    import mc_converter.logging_cfg
    assert mc_converter.logging_cfg
    logging.getLogger().debug('Default logging configuration is set up.')
import sys
from docopt import docopt
from mc_converter.release import MC_CONVERTER_VERSION as __version__

__updated__ = "2015-08-08"

logger = logging.getLogger(__name__)

class ArgsParserException(Exception):
    pass

def args_parser(argv=sys.argv[1:]):
    """
    Usage:
       mc_converter.py merge [options] [--] SCRIPT...
       mc_converter.py -h | --help | --version

       'merge' specific options:
       -o <outfile>, --output <outfile>         Output file [default: mc_converter.merge.out].
    """
    try:
        return docopt(args_parser.__doc__, argv=argv, version=__version__)
    except Exception as x:
        raise ArgsParserException('Failed to parse command line %s => x' % (argv, x))



def main(argv=sys.argv[1:]):
    try:
        args = args_parser(argv)
        logger.debug('Command line options:\n%s', args)
        logger.info('mc_converter, version %s', __version__)
        logger.debug('Current directory is "%s"', os.path.abspath(os.path.curdir))
        if args['merge']:
            merge(args['--output'], *args['SCRIPT'])
    except Exception as x:
        print('Error: %s' % x)
        sys.exit(1)

def merge(out_file_name, *scripts):
    import mc_converter.file_generator as fg
    logger.debug('Running merge to "%s"', out_file_name)
    fg.run_scripts(out_file_name, *scripts)

if __name__ == '__main__':
    main()

