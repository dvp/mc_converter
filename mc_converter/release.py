DISTNAME = "mc_converter"
DESCRIPTION = "Python modules for MCNP,TRIPOLY,Geant4 model building and processing"
MAINTAINER = "Dmitri Portnov"
MAINTAINER_EMAIL = "dmitri_portnov@yahoo.com"
URL = "http://gitlab.com/dvp2015/mc_converter"
LICENSE = "new BSD"
MC_CONVERTER_VERSION = "0.0.2"

__all__ = [MC_CONVERTER_VERSION, DISTNAME, DESCRIPTION, MAINTAINER, MAINTAINER_EMAIL, URL, LICENSE]