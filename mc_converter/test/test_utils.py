from contextlib import contextmanager
import os
import tempfile


@contextmanager
def using_temp_file(prefix, suffix):
    """Obtains and returns tempfile name. On context exit removes the file if exists"""
    td = tempfile.gettempdir()
    tf = tempfile.mktemp(suffix, prefix, td)
    if os.path.exists(tf):
        os.remove(tf)
    try:
        yield tf
    finally:
        if os.path.exists(tf):
            os.remove(tf)


