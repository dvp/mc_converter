from __future__ import print_function, division, absolute_import

import os
import tempfile

from numpy.testing import run_module_suite, assert_equal

from mc_converter.file_generator import InputManager, run_scripts
from mc_converter.utils import indir


# import logging
# import mc_converter.test.__test_logging_cfg
# logger = logging.getLogger(__name__)
# import numpy.testing.TestCase as TestCase
def test_file_generator():
    """Testing InputManager"""
    with indir(tempfile.gettempdir()):
        infile = 'test_inp.txt'
        outfile = 'test_out.txt'
        with open(infile, 'w') as f:
            print('Now is {__date__} {__time__}', file=f)
            print('Custom property is "{custom_property}"', end='', file=f)
        with InputManager(outfile) as imgr:
            imgr.props['custom_property'] = 'custom.true'
            imgr.include(infile)
            d = imgr.props['__date__']
            t = imgr.props['__time__']
            imgr.write('...')
        with open(outfile) as f:
            data = f.read()
            expected = 'Now is %s %s\nCustom property is "custom.true"...' % (d, t)
            assert_equal(data, expected)
        for f in [infile, outfile]:
            os.remove(f);

def test_file_generator_in_dir():
    """Testing InputManager with output to directory"""
    with indir(tempfile.gettempdir()):
        infile = 'test_inp.txt'
        outDir = 'out'
        if os.path.exists(outDir):
            os.rmdir(outDir)
        outfile = os.path.join(outDir, 'test_out.txt')
        for i in range(1, 2):  # check the cases when the directory present and not
            i = i
            with open(infile, 'w') as f:
                print('Now is {__date__} {__time__}', file=f)
                print('Custom property is "{custom_property}"', end='', file=f)
            with InputManager(outfile) as imgr:
                imgr.props['custom_property'] = 'custom.true'
                imgr.include(infile)
                d = imgr.props['__date__']
                t = imgr.props['__time__']
                imgr.write('...')
            assert os.path.isdir(outDir)
            with open(outfile) as f:
                data = f.read()
                expected = 'Now is %s %s\nCustom property is "custom.true"...' % (d, t)
                assert_equal(data, expected)
        for f in [infile, outfile]:
            os.remove(f);
        os.rmdir(outDir)


def test_run_scripts():
    with indir(tempfile.gettempdir()):
        file_to_include = "file_to_include.txt"
        with open(file_to_include, 'w') as f:
            print('Testing include of file {some_property}', file=f)
        python_file = "script_to_run.py"
        with open(python_file, 'w') as f:
            print("imgr.props['some_property'] = 'XXX'\nimgr.include('file_to_include.txt')", file=f)
        out_file = 'outfile.txt'
        run_scripts(out_file, python_file)
        with open(out_file, 'r') as f:
            actual = f.read()
            assert_equal(actual, 'Testing include of file XXX\n')

if __name__ == '__main__':
    import sys
    run_module_suite(sys.argv)
