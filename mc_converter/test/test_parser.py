from __future__ import print_function, division, absolute_import

import os.path
import sys
import tempfile

from numpy.testing import run_module_suite, assert_equal

import mc_converter.parser as p
import numpy.testing as tst


# from mc_converter.utils import indir
def build_data_file_name(name):
    return os.path.join(os.path.dirname(os.path.abspath(__file__)), 'data', name)

def test_build_data_file_name():
    actual = build_data_file_name('ng1')
    assert os.path.exists(actual)
    with open(actual) as f:
        s = next(f)
        assert s.startswith('message:')

def test_mcnp_sections_builder():
    b = p.mcnp_input_sections_builder()
    b.message = 'message: xxx'
    b.title = 'Testing title'
    b.cell_cards = '1 0 1'
    b.surface_cards = '1 so 0'
    b.control_cards = 'F4 1'
    s = b.build()
    assert_equal(s.message, 'message: xxx')
    assert_equal(s.title, 'Testing title')
    assert_equal(s.cell_cards, '1 0 1')
    assert_equal(s.control_cards, 'F4 1')
    assert s.remainder == None

def test_section_parser():
    fn = build_data_file_name('ng1')
    res = p.parse_sections(fn)
    assert res.message
    assert_equal(res.message, 'message: out=ooo')

if __name__ == '__main__':
    run_module_suite(sys.argv)
