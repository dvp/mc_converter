from __future__ import print_function, division, absolute_import

import sys
import tempfile

from numpy.testing import run_module_suite, assert_equal

from mc_converter.utils import indir
import mc_converter.main as m


def test_args_parser():
    argv = 'merge -o out/merge1.txt file1.py file2.py'.split()
    args = m.args_parser(argv)
    assert args['merge']
    assert_equal('out/merge1.txt', args['--output'])
    assert_equal(['file1.py', 'file2.py'], args['SCRIPT'])

def test_merge():
    with indir(tempfile.gettempdir()):
        file_to_include = "file_to_include.txt"
        with open(file_to_include, 'w') as f:
            print('Testing include of file {some_property}', file=f)
        python_file = "script_to_run.py"
        with open(python_file, 'w') as f:
            print("imgr.props['some_property'] = 'XXX'\nimgr.include('file_to_include.txt')", file=f)
        out_file = 'outfile.txt'
        m.main(['merge', '-o', out_file, python_file])
        with open(out_file, 'r') as f:
            actual = f.read()
            assert_equal(actual, 'Testing include of file XXX\n')

if __name__ == '__main__':
    run_module_suite(sys.argv)
