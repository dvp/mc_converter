try:  # Python 2
    from ConfigParser import ConfigParser
    from StringIO import StringIO
except ImportError:  # Python 3
    from configparser import ConfigParser
    from io import StringIO

#To avoid 'unused' warnings
if ConfigParser: pass
if StringIO: pass