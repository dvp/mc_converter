from contextlib import contextmanager
import os.path
import sys


def force_del(path):
    if not os.path.exists(path):
        return
    if os.path.isfile(path):
        os.remove(path)
        return
    for p in os.listdir(path):
        force_del(os.path.join(path, p))
    os.rmdir(path)

@contextmanager
def indir(path):
    orig = os.getcwd()
    os.chdir(path)
    yield
    os.chdir(orig)


@contextmanager
def cleanpypath(path):
    orig = sys.path
    sys.path = [p for p in sys.path if p != path]
    yield
    sys.path = orig


#===============================================================================
# def mkdirs(path):
#     p = path if isinstance(path, Path) else Path(path)
#     parent = p.parent
#     if parent and not parent.exists():
#         parent.mkdir(parents=True)
#===============================================================================
