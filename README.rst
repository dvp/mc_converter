
============================
Monte Carlo model converter
============================

----------------------------------------------------------------------------------------------------------
Package of tools to merge, split, convert and manipulate with MCNP, TIPOLI, Geant and Python code modules.
----------------------------------------------------------------------------------------------------------

Description
-----------

The python module supporting execution of a number of commands:
    * merge     - to build the output model as a result of specfied list of python scripts, which in turn can load text units, 
                 process them and store in the model using simple API provided with execution context
    * pythonize - from a given MCNP model build AST in python and save it for future use.
    * split     - pythonize a given MCNP, then split to text fragments applying user specified rules .
    * implify  - expand the surface expression, reduce and convert to the form minimizing the logical operations 
    * subtruct  - compute the expression for result of subtraction from one surface expression a number of others

The terminology, requirements and details of implementation are described in the following sections.

Abbreviations and definitions
-----------------------------
  * AST - abstract syntax tree
  * surface expression - MCNP definition of a cell using logical expression for combining volumes defined with surfaces (see 'Surface expression' section)

Requirements
------------
    General
    -------
    The module can be run by command python -m mc_converter
    Command line help is provided on running the module with option -h
    
    Non functional
    --------------
        * Compatible with Python version 2.7 and later

    Commands
    --------
* merge
    - run a specified list of python scripts to write the final model
        + the scripts are provided with a special context facilitating text unit processing
        + the context facilitates manipulations with context properties
        + text units processing includes resolving of string templates ({<name>} entries - the whole entry will be replaced with <name> property value)
        + final

Design specification
--------------------
**TBD**

Surface expressions
-------------------
**TBD**


TODO
----
Add benchmarks, contribution notes.